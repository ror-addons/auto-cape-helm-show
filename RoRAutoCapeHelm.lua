RoRAutoCapeHelm = {}

function RoRAutoCapeHelm.Initialize()
	RegisterEventHandler( SystemData.Events.PLAYER_ZONE_CHANGED , "RoRAutoCapeHelm.OnZoneChange");
	SetEquippedItemVisible( GameData.EquipSlots.HELM, true )
	SetEquippedItemVisible( GameData.EquipSlots.BACK, true )
end

function RoRAutoCapeHelm.OnZoneChange()
	SetEquippedItemVisible( GameData.EquipSlots.HELM , true)
	SetEquippedItemVisible( GameData.EquipSlots.BACK, true)
end