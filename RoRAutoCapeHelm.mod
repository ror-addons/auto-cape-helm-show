<?xml version="1.0" encoding="UTF-8"?>

<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">

    <UiMod name="Auto Cape/Helm Show" version="1.0" date="21/11/2014" >
		<Author name="Azarael" email="" />

        <VersionSettings gameVersion="1.4.8" windowsVersion="1.0"/>
		
		<Description text="Displays your cape and helm automatically when you zone." />

		<Files>
			<File name="RoRAutoCapeHelm.lua" />
		</Files>     
		
		<OnInitialize>
            <CallFunction name="RoRAutoCapeHelm.Initialize" />
		</OnInitialize>
    </UiMod>
	
</ModuleFile>